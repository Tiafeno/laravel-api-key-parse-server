<?php

namespace Cpp\LaravelApiKey\Providers;

use Cpp\LaravelApiKey\Console\Commands\ActivateApiKey;
use Cpp\LaravelApiKey\Console\Commands\DeactivateApiKey;
use Cpp\LaravelApiKey\Console\Commands\DeleteApiKey;
use Cpp\LaravelApiKey\Console\Commands\GenerateApiKey;
use Cpp\LaravelApiKey\Console\Commands\ListApiKeys;
use Cpp\LaravelApiKey\Http\Middleware\AuthorizeApiKey;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class ApiKeyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        $this->registerMiddleware($router);
        $this->publishes([
            __DIR__.'/../config/apikey.php' => config_path('apikey.php'),
        ], 'apikey-config');

        //ApiKey::observe(ApiKeyObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        $this->commands([
            ActivateApiKey::class,
            DeactivateApiKey::class,
            DeleteApiKey::class,
            GenerateApiKey::class,
            ListApiKeys::class,
        ]);

        $this->mergeConfigFrom(
            __DIR__.'/../config/apikey.php', 'apikey'
        );
    }

    /**
     * Register middleware
     *
     * Support added for different Laravel versions
     *
     * @param Router $router
     */
    protected function registerMiddleware(Router $router)
    {
        $versionComparison = version_compare(app()->version(), '5.4.0');

        if ($versionComparison >= 0) {
            $router->aliasMiddleware('auth.apikey', AuthorizeApiKey::class);
        } else {
            $router->middleware('auth.apikey', AuthorizeApiKey::class);
        }
    }

}

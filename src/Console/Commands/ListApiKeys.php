<?php

namespace Cpp\LaravelApiKey\Console\Commands;

use DateTime;
use DateTimeInterface;
use Cpp\LaravelApiKey\Models\ApiKey;
use Illuminate\Console\Command;
use Parse\ParseObject;

class ListApiKeys extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apikey:list {--D|deleted}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all API Keys';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $keys = $this->option('deleted')
            ? ApiKey::withTrashed()->orderBy('name')->get()
            : ApiKey::getAPIs('name');

        if (count($keys) === 0) {
            $this->info('There are no API keys');
            return;
        }

        $headers = ['Name', 'ID', 'Status', 'Date', 'Key'];
        $rows = collect($keys)->map(function(ParseObject $key) {
            $status = $key->active    ? 'active'  : 'disabled';
            $updateAtDatetime = $key->getUpdatedAt();
            return [
                $key->name,
                $key->getObjectId(),
                $status,
                $updateAtDatetime->format(DateTimeInterface::RFC822),
                $key->key
            ];
        });
        $this->table($headers, $rows);
    }
}

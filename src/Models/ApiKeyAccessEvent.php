<?php

namespace Cpp\LaravelApiKey\Models;

use Parziphal\Parse\ObjectModel;

class ApiKeyAccessEvent extends ObjectModel
{
    protected $table = 'ApiKeyAccessEvent';
    protected static $defaultUseMasterKey = true;
    /**
     * Get the related ApiKey record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function apiKey()
    {
        return $this->belongsTo(ApiKey::class, 'api_key_id');
    }

}

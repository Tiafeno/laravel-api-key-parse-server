<?php

namespace Cpp\LaravelApiKey\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parziphal\Parse\ObjectModel;

class ApiKey extends ObjectModel
{
    use SoftDeletes;

    const EVENT_NAME_CREATED = 'created';
    const EVENT_NAME_ACTIVATED = 'activated';
    const EVENT_NAME_DEACTIVATED = 'deactivated';
    const EVENT_NAME_DELETED = 'deleted';

    protected static $defaultUseMasterKey = true;
    protected static $nameRegex = '/^[a-z0-9-]{1,255}$/';

    protected $table = 'ApiKey';

    public static function getInstance()
    {
        return new self;
    }

    /**
     * Get the related ApiKeyAccessEvents records
     *
     * @return \Parziphal\Parse\Relations\HasMany
     */
    public function accessEvents()
    {
        return $this->hasMany(ApiKeyAccessEvent::class, 'api_key_id');
    }

    /**
     * Get the related ApiKeyAdminEvents records
     *
     * @return \Parziphal\Parse\Relations\HasMany
     */
    public function adminEvents()
    {
        return $this->hasMany(ApiKeyAdminEvent::class, 'api_key_id');
    }

    /**
     * Generate a secure unique API key
     *
     * @return string
     */
    public static function generateKey()
    {
        do {
            $key = Str::random(64);
        } while (self::keyExists($key));

        return $key;
    }

    /**
     * Generate a key/secret pair
     *
     * @return array
     */
    public static function generateSecret()
    {
        return Str::random(64);
    }

    /**
     * Get ApiKey record by key value
     *
     * @param string $key
     * @return ParseObject|null
     */
    public static function getByKey($key)
    {
        $query = new ParseQuery(self::getInstance()->getTable());
        return $query->equalTo('key', $key)->equalTo('active', 1)->first(true);
    }

    /**
     * @param string $orderby
     * @param int $limit -  Default 10
     * @return \Parse\Array|ParseObject[]
     */
    public static function getAPIs($orderby = "name", $limit = 10)
    {
        $query = new ParseQuery(self::getInstance()->getTable());
        return $query->ascending($orderby)->limit($limit)->find(true);
    }

    /**
     * Check if key is valid
     *
     * @param string $key
     * @return bool
     */
    public static function isValidKey($key)
    {
        return self::getByKey($key) instanceof self;
    }

    /**
     * Check if name is valid format
     *
     * @param string $name
     * @return bool
     */
    public static function isValidName($name)
    {
        return (bool)preg_match(self::$nameRegex, $name);
    }

    /**
     * Check if a key already exists
     *
     * Includes soft deleted records
     *
     * @param string $key
     * @return bool
     */
    public static function keyExists($key): bool
    {
        $apiQuery = new ParseQuery(self::getInstance()->getTable());
        $result = $apiQuery->equalTo('key', $key)->first(true);
        return $result instanceof ParseObject;
    }

    /**
     * Check if a name already exists
     *
     * Does not include soft deleted records
     *
     * @param string $name
     * @return bool
     */
    public static function nameExists($name)
    {
        $apiQuery = new ParseQuery(self::getInstance()->getTable());
        $result = $apiQuery->equalTo('name', $name)->first(true);
        return $result instanceof ParseObject;
    }

    public function getTable()
    {
        return $this->table;
    }

    /**
     * Log an API key admin event
     *
     * @param ApiKey $apiKey
     * @param string $eventName
     */
    protected static function logApiKeyAdminEvent(ApiKey $apiKey, $eventName)
    {
        $event = new ApiKeyAdminEvent;
        $event->api_key_id = $apiKey->id;
        $event->ip_address = request()->ip();
        $event->event = $eventName;
        $event->save();
    }
}

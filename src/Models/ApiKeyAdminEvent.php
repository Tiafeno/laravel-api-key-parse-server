<?php

namespace Cpp\LaravelApiKey\Models;

use Parziphal\Parse\ObjectModel;

class ApiKeyAdminEvent extends ObjectModel
{
    protected $table = 'ApiKeyAdminEvent';
    protected static $defaultUseMasterKey = true;
    /**
     * Get the related ApiKey record
     *
     * @return \Parziphal\Parse\Relations\BelongsTo
     */
    public function apiKey()
    {
        return $this->belongsTo(ApiKey::class, 'api_key_id');
    }

}

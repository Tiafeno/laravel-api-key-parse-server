<?php

namespace Cpp\LaravelApiKey\Http\Middleware;

use Closure;
use Cpp\LaravelApiKey\Models\ApiKey;
use Cpp\LaravelApiKey\Models\ApiKeyAccessEvent;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Parse\ParseObject;

class AuthorizeApiKey
{
    const AUTH_HEADER = 'X-Authorization';
    const AUTH_SECRET = 'X-Authorization-Secret';

    /**
     * Handle the incoming request
     *
     * @param Request $request
     * @param Closure $next
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next)
    {
        $header = $request->header(self::AUTH_HEADER);
        $secret = $request->header(self::AUTH_SECRET);
        $apiKey = ApiKey::getByKey($header);
        if (is_null($apiKey)  || !$apiKey instanceof ParseObject) {
            return response([
                'errors' => 'Key not found'
            ], 401);
        }
        if ($apiKey instanceof ApiKey && $this->isValid($secret, $apiKey)) {
            $this->logAccessEvent($request, $apiKey);
            return $next($request);
        }
        return response([
            'errors' => [[
                'message' => 'Unauthorized'
            ]]
        ], 401);
    }

    /**
     * Test the secret key, but only if it is configured to be used
     *
     * @param string $secret
     * @param ApiKey $apiKey
     * @return boolean
     */
    public function isValid($secret, ApiKey $apiKey) {
        if($secret && $secret === $apiKey->secret) {
            // configured and passes
            return true;
        } else {
            // configured, but failed
            return false;
        }
    }

    /**
     * Log an API key access event
     *
     * @param Request $request
     * @param ApiKey  $apiKey
     */
    protected function logAccessEvent(Request $request, ParseObject $apiKey)
    {
        ApiKeyAccessEvent::create([
            'api_key_id' => $apiKey->getObjectId(),
            'id_address' => $request->ip(),
            'url' => $request->fullUrl()
        ], true);
    }
}
